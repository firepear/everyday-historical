package main

import (
	"time"
)

// getTime takes ISO8601 date and time fragments and returns a
// time.Time object based on them.
func getTime(d string, t string) (time.Time, error) {
	if t != "" {
		d = d + "T" + t + ":00Z"
	} else {
		d = d + "T23:59:59Z"
	}
	ptime, err := time.Parse(time.RFC3339, d)
	if err != nil { return ptime, err }
	return ptime, err
}
