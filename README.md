everyday-historical
===================

*This is the original, standalone implementation of everyday. Work is underway on a new client/server version. This version should be considered end-of-lifed.*

----

A work tracking calendar with project diaries.

Read this article: http://lifehacker.com/281626/jerry-seinfelds-productivity-secret

If that sounds like a good idea, and you're the sort of person who
lives at a shell prompt, then `everyday` may be right for you.



How to get it
-------------

It's written in Go. If you don't already have a configured Go
workspace,
[make yourself one](http://golang.org/doc/code.html#Workspaces), then
do:

`go get bitbucket.org/firepear/everyday-historical`



What you can do
---------------

TL;DR: run `everyday -h`

----

To see the calendar, just run `everyday`.

The current day will be [bracketed]. Days you have worked will be in
reverse video. A chain of days worked which includes today or
yesterday (a current chain) will be colored cyan.

To tell `everyday` that you've done work, do

`everyday +`

to add a diary entry about whatever you're doing right this
minute. Your favorite editor (or `vi` if you don't have a favorite)
will pop up. Type in whatever you'd like to say about the work you've
been doing, then save and quit.

You can categorize entries by adding a project, task, etc. name.

`everyday + <proj_name>`

If you want to see the calendar with the days worked for just one
project displayed, do

`everyday <proj_name>`

To remind yourself of what projects you have entries for, see

    everyday ?

----

To see diary entries from the most recent streak (optionally for a specific project),

    everyday ^ [proj_name]

Or to see entries made yesterday,

    everyday yesterday [proj_name]

And similarly, to see entries from the past week,

    everyday week [proj_name]

----

If you want to backdate an entry, just specify the time when it should
have been added:

    everyday + <hh:mm> [proj_name]

Or retroactively add an entry for another day:

    everyday + yesterday [proj_name]
    everyday + <yyyy-mm-dd> [proj_name]
    everyday + [yyyy-mm-dd] [hh:mm] [proj_name]

The order [things] appear in doesn't matter.

N.B. Project names must match `^[A-Za-z][\w\-]+$` (start with a
letter; then one or more letters, numbers, hyphens, and/or
underscores).
