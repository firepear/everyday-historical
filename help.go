package main

import "fmt"

func printHelp() {
	fmt.Printf(`Usage: <values> are required; [values] are optional

Show work streak calendar          everyday
Show streaks for a specific year   everyday <yyyy>
Show streaks for a single project  everyday <proj_name> [yyyy]

Add diary entry              everyday + [proj_name]
Add entry for earlier today  everyday + [proj_name] <hh:mm>
Add entry for yesterday      everyday + [proj_name] yesterday
Add entry for earlier day    everyday + [proj_name] <yyyy-mm-dd> [hh:mm]

List known projects  everyday ?

Show entries from current streak   everyday ^
Show entries from yesterday        everyday [proj_name] yesterday
Show entries from the past week    everyday [proj_name] week
`)
//Show entries from a project        everyday ^ <proj_name>
//Show entries from a specific day   everyday ^ <yyyy-mm-dd>
//Show entries from specific month   everyday [proj_name] <month_num>
//Show entries from > 12 months ago  everyday [proj_name] <yyyy-mm>
}

func printVersion() {
	fmt.Printf("everyday %v\n", version)
}
