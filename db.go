package main

import (
	"database/sql"
	"fmt"
	"os"
	_ "github.com/mattn/go-sqlite3"
)

// work holds data about which days have been worked for a given year,
// and also the Julian dates of the head and tail of the last streak
// for that year.
type work struct {
	workdays        map[int]bool
    laststreak      [2]int
}

// entry represents one diary entry for/from the backing datastore.
type entry struct {
	id      int64
	year    int
	day     int
	ts      int64
	project string
	text    string
}


var db *sql.DB
var dbfile = os.Getenv("HOME") + "/.everydaydb"
var createsql = `
CREATE TABLE entries (id integer primary key autoincrement, year integer, day integer, ts integer, entry text, project text);
CREATE INDEX yidx on entries (year);
CREATE INDEX pidx on entries (project);
`

// initDB asks for the creation of the SQLite db which is our backing
// store if it does not exist, and always opens a connection to it.
func initDB() (err error) {
	_, err = os.Stat(dbfile)
	if err != nil {
		err = createDB()
	}
	if err != nil { return }
	db, err = sql.Open("sqlite3", dbfile)
	return
}


// createDB runs the SQL commands which instantiate the db.
func createDB() (err error) {
	db, err := sql.Open("sqlite3", dbfile)
	if err != nil {
		err = fmt.Errorf("Couldn't open db: %v\n", err)
		return
	}
	defer db.Close()
	_, err = db.Exec(createsql)
	if err != nil {
		err = fmt.Errorf("Failed to execute DB creation script: %v\n", err)
	}
	return
}


// getWork queries the database and returns information about days
// worked in a given year.
func getWork(a *arguments) (w work, err error) {
	var rows *sql.Rows
	w.workdays = make(map[int]bool)
	// we may or may not be filtering by project
	if a.project != "" {
		rows, err = db.Query(`SELECT day FROM entries WHERE year = ? AND project LIKE ? ORDER BY day`, a.year, a.project + "%")
	} else {
		rows, err = db.Query("SELECT day FROM entries WHERE year = ? ORDER BY day", a.year)
	}
	if err != nil { return }
	var wdk []int // a list of keys of the workdays map
	for rows.Next() {
		var d int
		if err = rows.Scan(&d); err != nil { return }
		w.workdays[d] = true
		wdk = append(wdk, d)
	}
	if len(w.workdays) == 0 { return }
	// reverse wdk
	//sort.Sort(sort.Reverse(sort.IntSlice(wdk)))
	for i, j := 0, len(wdk)-1; i < j; i, j = i+1, j-1 {
		wdk[i], wdk[j] = wdk[j], wdk[i]
	}
	// set the head and tail of laststreak to the first element of wdk
	// (which is reversed, so it's the last day worked)
	w.laststreak[0] = wdk[0]
	w.laststreak[1] = wdk[0]
	// step through wdk. doy is "day of year"
	for _, doy := range wdk {
		// if day of year is more than 1 day from the first day of the
		// streak, then the streak has ended and we break the loop
		if w.laststreak[0] - doy > 1 {
			break
		} else {
			// otherwise, this day is part of the streak, so move the
			// beginning of the streak back one day
			w.laststreak[0] = doy
		}
	}
	return
}

// insertEntry takes an entry struct and inserts it into the database.
func insertEntry(e *entry) error {
	stmt, err := db.Prepare(`INSERT INTO entries(year, day, ts, entry, project) VALUES(?, ?, ?, ?, ?)`)
	if err != nil { return err }
	_, err = stmt.Exec(e.year, e.day, e.ts, e.text, e.project)
	return err
}

// getProjectList returns the list of extant projects
func getProjectList() ([]string, error) {
	var rows *sql.Rows
	var projl []string

	rows, err := db.Query(`SELECT DISTINCT(project) FROM entries WHERE project <> "" ORDER BY project`)
	if err != nil { return projl, err }
	for rows.Next() {
		var proj string
		if err = rows.Scan(&proj); err != nil { return projl, err }
		projl = append(projl, proj)
	}
	return projl, err
}

// getEnties returns a list of entries based on argument criteria
func getEntries(a *arguments) ([]*entry, error) {
	var rows *sql.Rows
	var entries []*entry
	var err error
	
	switch {
	case a.tspecial == "yesterday":
		if a.project == "" {
			rows, err = db.Query(`SELECT id, year, day, ts, project, entry FROM entries WHERE ts >= ? AND ts <= ? ORDER BY ts`, a.now.Unix(), a.now.Unix() + 86399)
			if err != nil { return entries, err}
		} else {
			rows, err = db.Query(`SELECT id, year, day, ts, project, entry FROM entries WHERE project LIKE ? AND ts >= ? AND ts <= ? ORDER BY ts`, a.project, a.now.Unix(), a.now.Unix() + 86399)
			if err != nil { return entries, err}
		}
	case a.tspecial == "week":
		if a.project == "" {
			rows, err = db.Query(`SELECT id, year, day, ts, project, entry FROM entries WHERE ts >= ? AND ts <= ? ORDER BY ts`, a.now.Unix(), a.now.Unix() + (86400 * 7 - 1))
			if err != nil { return entries, err}
		} else {
			rows, err = db.Query(`SELECT id, year, day, ts, project, entry FROM entries WHERE project LIKE ? AND ts >= ? AND ts <= ? ORDER BY ts`, a.project, a.now.Unix(), a.now.Unix() + (86400 * 7 - 1))
			if err != nil { return entries, err}
		}
	case a.month[0] == "" && a.project == "":
		// if month[0] and project are blank, get entries from this streak
		w, err := getWork(a)
		rows, err = db.Query(`SELECT id, year, day, ts, project, entry FROM entries WHERE year = ? AND day >= ? AND day <= ? ORDER BY ts`, a.year, w.laststreak[0], w.laststreak[1])
		if err != nil { return entries, err}
	}
	for rows.Next() {
		var e entry
		if err = rows.Scan(&e.id, &e.year, &e.day, &e.ts, &e.project, &e.text); err != nil { return entries, err }
		entries = append(entries, &e)
	}
	return entries, err
}
