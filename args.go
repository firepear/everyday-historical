package main

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// arguments represents the arguments entered by the user, and
// derivative date generated from those arguments during parsing
// and/or validation. It is hte primary driver for the rest of the
// program.
type arguments struct {
	now        time.Time
	command    string
	tspecial   string
	year       int
	isLeap     bool
    month      [2]string
	day        string
	time       string
	date       string
	yrday      int
	isThisYear bool
	project    string
	cols       int
	rows       int
}

var cmdRegex   = regexp.MustCompile(`^[\+\-\=\^\?]$`)
var tspecRegex = regexp.MustCompile(`^(week|yesterday)$`)
var monthRegex = regexp.MustCompile(`^\d{1,2}$`)
var yearRegex  = regexp.MustCompile(`^\d{4}$`)
var yrmonRegex = regexp.MustCompile(`^\d{4}\-\d{2}$`)
var ymdRegex   = regexp.MustCompile(`^\d{4}\-\d{2}\-\d{2}$`)
var timeRegex  = regexp.MustCompile(`^\d{2}:\d{2}$`)
var projRegex  = regexp.MustCompile(`^[A-Za-z][\w\-]+$`)


// parseArgs takes the arguments from the command line and plugs them
// into an `arguments` struct.
func parseArgs(a []string) (p arguments, err error) {
	for _, arg := range a {
		// mostly we just make sure we only have one of any possible type of argument
		switch {
		case cmdRegex.MatchString(arg):
			if p.command != "" {
				err = fmt.Errorf("Dupe command '%v' (already have '%v')", arg, p.command)
				break
			}
			p.command = arg
		case tspecRegex.MatchString(arg):
			if p.tspecial != "" {
				err = fmt.Errorf("Dupe special time '%v' (already have '%v')", arg, p.tspecial)
				break
			}
			p.tspecial = arg
		case monthRegex.MatchString(arg):
			if p.month[1] != "" {
				err = fmt.Errorf("Dupe month '%v' (already have '%v')", arg, p.month[1])
				break
			}
			p.month[1] = fmt.Sprintf("%02s", arg)
		case yearRegex.MatchString(arg):
			if p.year != 0 {
				err = fmt.Errorf("Dupe year '%v' (already have '%v')", arg, p.year)
				break
			}
			p.year, _ = strconv.Atoi(arg)
		case yrmonRegex.MatchString(arg), ymdRegex.MatchString(arg):
			// but if we're looking at a year-month or year-mn-dy,
			// then things get a little more complicated.

			// first do the usual sort of checks
			if p.year != 0 {
				err = fmt.Errorf("You entered a year and month, but year is already set to '%v'", p.year)
				break
			}
			if p.month[1] != "" {
				err = fmt.Errorf("You entered a year and month, but month is already set to '%v'", p.month[1])
				break
			}
			// split the y-m (or y-m-d) into a slice
			tmpym         := strings.Split(arg, "-")
			p.month[0] = tmpym[0] // the year part of .month
			p.month[1] = tmpym[1] // the month part
			// convert year to an int and slug it into .year
			p.year, _  = strconv.Atoi(tmpym[0])
			// and if we matched ymdRegex, set date too
			if len(tmpym) == 3 {
				p.date = arg
				p.day = tmpym[2]
			}
		case timeRegex.MatchString(arg):
			if p.time != "" {
				err = fmt.Errorf("Dupe time '%v' (already have '%v')", arg, p.time)
				break
			}
			p.time = arg
		case projRegex.MatchString(arg):
			if p.project != "" {
				err = fmt.Errorf("Dupe project '%v' (already have '%v')", arg, p.project)
				break
			}
			p.project = arg
		default:
			err = fmt.Errorf("Unrecognized argument '%v'", arg)
			break
		}
	}
	// set command to lookup if we have a date or month but no command
	if p.command == "" && (p.date != "" || p.month[1] != "" || p.tspecial != "") {
		p.command = "^"
	}
	switch p.tspecial {
	case "week":
		// fake "now" being 6 days ago
		if p.command != "^" {
			err = fmt.Errorf(`Special time "week" only makes sense when doing a lookup`)
			break
		}
		ttime := time.Unix(time.Now().Unix() - (86400 * 6), 0)
		_, to := time.Now().Zone()
		p.now, _ = time.Parse(time.RFC3339, fmt.Sprintf("%d-%02d-%02dT00:00:00%+03d:00", ttime.Year(), ttime.Month(), ttime.Day(), to / 3600))
	case "yesterday":
		// fake "now" being yesterday
		ttime := time.Unix(time.Now().Unix() - 86400, 0)
		_, to := time.Now().Zone()
		p.now, _ = time.Parse(time.RFC3339, fmt.Sprintf("%d-%02d-%02dT00:00:00%+03d:00", ttime.Year(), ttime.Month(), ttime.Day(), to / 3600))
	default:
		p.now = time.Now()
	}
	return
}

func validateArgs(a *arguments) (err error) {
	gmonth, _ := strconv.Atoi(a.month[1])                            // given month
	nmonth, _ := strconv.Atoi(fmt.Sprintf("%d", a.now.Month() + 1))  // "now" month
	gday, _ := strconv.Atoi(a.day)
	// a time only makes sense with a fully qualified date OR alone
	if a.time != "" {
		if a.date == "" {
			if  a.year != 0 || a.month[1] != "" {
				err = errors.New("Specifying a time with a fragmentary date makes no sense")
				return
			}
		}
	}
	// make sure month is a real month
	if a.month[1] != "" {
		if gmonth > 12 || gmonth < 1 {
			err = fmt.Errorf("'%v' is not a valid month of the year", gmonth)
			return
		}
	}
	// if there is no year set, but month *is* set, AND the month is
	// in the future from now's point of view, set year to the
	// previous year.
	if a.year == 0 && a.month[0] == "" && a.month[1] != "" {
		if gmonth > nmonth {
			a.year = a.now.Year() - 1
		}
	}
	// set year if still not set
	if a.year == 0 {
		a.year = a.now.Year()
	}
	// see is a.year is the current year
	if a.year == a.now.Year() {
		a.isThisYear = true
	}
	// ensure given date is not in future
	future := false
	if a.year > a.now.Year() {
		future = true
	}
	if a.year == a.now.Year() && gmonth > nmonth {
		future = true
	}
	if a.year == a.now.Year() && gmonth == nmonth {
		future = true
	}
	if future {
		err = errors.New("The future hasn't happened yet.")
		return
	}
	// check for leapyears
	ldoyf := fmt.Sprintf("%v-12-31T00:00:00Z", a.year)
	ldoyt, _ := time.Parse(time.RFC3339, ldoyf)
	if ldoyt.YearDay() > 365 {
		a.isLeap = true
	}
	// make sure day is a real day
	if a.day != "" {
		switch gmonth {
		default:
			if gday > 31 {
				err = fmt.Errorf("'%v' is not a valid day", a.date)
			}
		case 4, 6, 9, 11:
			if gday > 30 {
				err = fmt.Errorf("'%v' is not a valid day", a.date)
			}
		case 2:
			if a.isLeap {
				if gday > 29 {
					err = fmt.Errorf("'%v' is not a valid day", a.date)
				}
			} else {
				if gday > 28 {
					err = fmt.Errorf("'%v' is not a valid day", a.date)
				}
			}
		}
	}
	if err != nil { return }
	// set yrday
	if a.date == "" {
		a.yrday = a.now.YearDay()
	} else {
		tmptime, err := getTime(a.date, a.time)
		if err != nil { return err }
		a.yrday = tmptime.YearDay()
	}
	return
}
