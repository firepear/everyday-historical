package main

import (
	"errors"
)

// doCommand is a dispatch routine which runs the appropriate routine
// for the given command.
func doCommand(p *arguments, a []string) (err error) {
	err = initDB()
	if err != nil { return }
	switch p.command {
	case "+":
		err = newDiaryEntry(p)
	case "^":
		err = printEntries(p)
	case "?":
		err = lookupProjectList()
	case "-":
		err = errors.New("Deletion not yet implemented")
	case "=":
		err = errors.New("Editing not yet implemented")
	default:
		err = drawCalendar(p)
	}
	return
}
