package main

import (
	"fmt"
	"flag"
	"os"
)

var version = "0.1.15"
var showhelp bool
var showver bool

func init() {
	flag.BoolVar(&showhelp, "?", false, "Show help")
	flag.BoolVar(&showhelp, "h", false, "Show help")
	flag.BoolVar(&showhelp, "help", false, "Show help")
	flag.BoolVar(&showver, "v", false, "Show ver")
	flag.BoolVar(&showver, "version", false, "Show ver")
}

func main() {
	flag.Parse()
	if showhelp {
		printHelp()
		os.Exit(0)
	}
	if showver {
		printVersion()
		os.Exit(0)
	}

	args := flag.Args()
	pargs, err := parseArgs(args)
	if err != nil {
		fmt.Printf("Error parsing arguments: %v\n", err)
		os.Exit(1)
	}
	err = validateArgs(&pargs)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(1)
	}

	err = doCommand(&pargs, args)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(1)
	}
}
