package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/user"
	"time"
)

// newDiaryEntry constructs a diary entry by spawning an editor
// session, reading in the resulting file, and populating other fields
// from the arguments struct.
func newDiaryEntry(a *arguments) (error) {
	// get username
	me, err := user.Current()
	if err != nil { return err }
	// set our temp filename
	tmpfile := os.TempDir() + "/" + me.Username + fmt.Sprintf("%v", a.now.Unix())
	// get user's preferred editor
	editor := os.Getenv("VISUAL")
	if editor == "" {
		editor = os.Getenv("EDITOR")
	}
	if editor == "" {
		editor = "vi"
	}
	// run editor
	cmd := exec.Command(editor, tmpfile)
	cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
	err = cmd.Run()
	if err != nil { return err }
	// instantiate entry
	var e entry
	e.year, e.day, e.project = a.year, a.yrday, a.project
	// read in entry text
	textbytes, err := ioutil.ReadFile(tmpfile)
	if err != nil { return err }
	// some editors may save a zero-length file
	if len(textbytes) == 0 {
		err = errors.New("No text was entered -- diary entry is empty.")
		return err
	}
	e.text = string(textbytes)
	// set timestamp
	if a.date == "" {
		e.ts = a.now.Unix()
	} else {
		tmptime, err := getTime(a.date, a.time)
		if err != nil { return err }
		e.ts = tmptime.Unix()
	}
	// insert into db
	err = insertEntry(&e)
	if err == nil { _ = os.Remove(tmpfile) }
	return err
}


// printEntries fetches entries and 
func printEntries(a *arguments) error {
	entries, err := getEntries(a)
	if err != nil { return err }
	for _, e := range entries {
		if e.project == "" { e.project = "No project" }
		t := time.Unix(e.ts, 0)
		y, m, d := t.Date()
		fmt.Println()
		fmt.Printf("%v\t%v-%02d-%02v %02v:%02v\n", e.project, y, m, d, t.Hour(), t.Minute())
		fmt.Printf("=============================================================================\n")
		fmt.Println(e.text)
	}
	return err
}

// lookupProjectList fetches the list of extant project names
func lookupProjectList() error {
	projects, err := getProjectList()
	if err != nil { return err }
	for _, proj := range projects {
		fmt.Println(proj)
	}
	return err
}
