package main

import (
	"fmt"
)

var months = map[int]string{
	1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr",
	5: "May", 6: "Jun", 7: "Jul", 8: "Aug",
	9: "Sep", 10: "Oct", 11: "Nov", 12: "Dec",
}

// drawCalendar gets a one-year block of days worked from the
// database, and displays it in a calendar format.
func drawCalendar(a *arguments) (err error){
	var ao string // ANSI opening
	var ac string // ANSI closing
	var bo string // bracket open
	var bc string // bracket close

	// get a year's work from the db
	w, err := getWork(a)
	if err != nil { return }

	fmt.Printf("\n\033[1m%v\033[0m\n\n", a.year)
	daycount := 1 // day-of-year for the calendar we're printing out
	for month := 1; month <= 12; month++ {
		fmt.Printf(" \033[1m%v\033[0m  ", months[month])
		var days int
		switch month {
		default:
			days = 31
		case 4, 6, 9, 11:
			days = 30
		case 2:
			if a.isLeap {
				days = 29
			} else {
				days = 28
			}
		}
		for d := 1; d <= days; d++ {
			ao, ac, bo, bc = "", "", " ", " "
			if _, ok := w.workdays[daycount]; ok {
				// if the today's day-of-year minus the last day of
				// the last streak is 0 or 1 AND the calendar's
				// day-of-year is >= the first day of the last streak,
				// AND we're looking at the current year, then the
				// *last* streak is also the *current* streak
				if (a.yrday - w.laststreak[1] <= 1) && daycount >= w.laststreak[0] && a.isThisYear == true {
					ao = "\033[36;7m"
					ac = "\033[0m"
				} else {
					// just a plain old streak
					ao = "\033[7m"
					ac = "\033[0m"
				}
			}
			// put brackets around today
			if daycount == a.yrday && a.isThisYear {
				bo = "["
				bc = "]"
			}
			fmt.Printf("%s%s%02d%s%s", ao, bo, d, bc, ac)
			daycount++
		}
		fmt.Printf("\n\n")
	}
	return
}
